# Shopping Cart Microservice
Project created for recruitment purposes.

## Requirements

* App should not lose messages if database connection goes down
* App should be able to automatically create table if it doesn't exist in the database
* App should self-heal from database connection issues without human interaction
* App should be fault tolerant
* App should have good test coverage

## Infrastructure

* App requires PostgreSQL database running (configurable via environment variables, for reference check `src/main/resources/application.yml`) 
* App makes use of embedded ActiveMQ as a JMS broker

## Problem Solution

* App uses a configurable redelivery policy for ActiveMQ with sane defaults for simple non-blocking retrial in case of short database connection breaks
* In case of failed redelivery, messages and up in dead letter queue (ActiveMQ default DLQ is used for simplicity)
* App uses a scheduled mechanism to check database health status using Spring's actuator and publishes an application event indicating that database connection has been resumed
(for reference take a look at `DatabaseHealthMessagePublisher`)
* Another component listens for above messages, browses through the dead letter queue and republishes all of the messages to the main queue (for reference take a look at `JmsRecoveryHandler`)
* Combination of above provides a reliable mechanism which assures that all messages will be persisted no matter if the database is up at first or any future acquisitions
* App exposes simple REST endpoints to send some form of mock data to the JMS queue and observe application behaviour depending on database connection state (for reference take a look at `ShoppingCartMockController`)
* App requires the database to be up at the start-up time (Caused by the requirement about table creation which gets executed as soon as application launches)
* The "self-heal", redelivery and domain parts are 100% unit tested, with some edge cases (default code coverage reporting tool indicates that overall test coverage for project is around 70%, it is due to the fact that a good portion of code is a mock controller for demo purposes and configuration classes)
