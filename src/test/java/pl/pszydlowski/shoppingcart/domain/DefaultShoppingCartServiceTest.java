package pl.pszydlowski.shoppingcart.domain;

import org.junit.Test;
import pl.pszydlowski.shoppingcart.domain.DefaultShoppingCartService;
import pl.pszydlowski.shoppingcart.port.persistance.ShoppingCartProductRepository;

import static org.mockito.ArgumentMatchers.argThat;
import static org.mockito.BDDMockito.then;
import static org.mockito.Mockito.mock;

public class DefaultShoppingCartServiceTest {

    private final ShoppingCartProductRepository repository = mock(ShoppingCartProductRepository.class);
    private final DefaultShoppingCartService service = new DefaultShoppingCartService(repository);

    @Test
    public void should_save_product_to_repository() {
        //given
        String productId = "product-id";

        //when
        service.persistProduct(productId);

        //then
        then(repository).should().save(argThat(cart -> cart.getProductId().equals(productId)));
    }
}
