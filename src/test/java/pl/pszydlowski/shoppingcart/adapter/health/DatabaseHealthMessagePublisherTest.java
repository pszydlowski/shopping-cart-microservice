package pl.pszydlowski.shoppingcart.adapter.health;

import org.junit.Test;
import org.springframework.boot.actuate.health.Health;
import org.springframework.boot.actuate.jdbc.DataSourceHealthIndicator;
import org.springframework.context.ApplicationEventPublisher;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.then;
import static org.mockito.Mockito.mock;

public class DatabaseHealthMessagePublisherTest {

    private final DataSourceHealthIndicator indicator = mock(DataSourceHealthIndicator.class);
    private final ApplicationEventPublisher publisher = mock(ApplicationEventPublisher.class);
    private final DatabaseHealthMessagePublisher healthMessagePublisher = new DatabaseHealthMessagePublisher(indicator, publisher);

    @Test
    public void should_indicate_that_database_is_active_by_default() {
        //expect
        assertThat(healthMessagePublisher.isDatabaseActive()).isTrue();
    }

    @Test
    public void should_indicate_that_database_went_down() {
        //given
        given(indicator.health()).willReturn(Health.down().build());

        //when
        healthMessagePublisher.checkDatabaseHealth();

        //then
        assertThat(healthMessagePublisher.isDatabaseActive()).isFalse();
    }

    @Test
    public void should_indicate_that_database_went_up_and_publish_event() {
        //given
        healthMessagePublisher.setDatabaseActive(false);
        given(indicator.health()).willReturn(Health.up().build());

        //when
        healthMessagePublisher.checkDatabaseHealth();

        //then
        assertThat(healthMessagePublisher.isDatabaseActive()).isTrue();
        then(publisher).should().publishEvent(new DatabaseConnectionResumedEvent(healthMessagePublisher));
    }

    @Test
    public void should_indicate_no_change_when_database_was_previously_and_still_is_up() {
        //given
        given(indicator.health()).willReturn(Health.up().build());

        //when
        healthMessagePublisher.checkDatabaseHealth();

        //then
        assertThat(healthMessagePublisher.isDatabaseActive()).isTrue();
    }

    @Test
    public void should_indicate_no_change_when_database_was_previously_and_still_is_down() {
        //given
        healthMessagePublisher.setDatabaseActive(false);
        given(indicator.health()).willReturn(Health.down().build());

        //when
        healthMessagePublisher.checkDatabaseHealth();

        //then
        assertThat(healthMessagePublisher.isDatabaseActive()).isFalse();
    }
}
