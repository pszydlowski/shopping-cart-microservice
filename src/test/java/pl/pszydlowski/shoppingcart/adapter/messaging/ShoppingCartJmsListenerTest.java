package pl.pszydlowski.shoppingcart.adapter.messaging;

import org.junit.Test;
import pl.pszydlowski.shoppingcart.port.messaging.ShoppingCartMessage;
import pl.pszydlowski.shoppingcart.domain.ShoppingCartService;

import static org.mockito.Mockito.mock;

public class ShoppingCartJmsListenerTest {

    private final ShoppingCartService service = mock(ShoppingCartService.class);
    private final ShoppingCartJmsListener listener = new ShoppingCartJmsListener(service);

    @Test
    public void should_call_service_on_new_message() {
        //given
        String productId = "product";
        ShoppingCartMessage message = new ShoppingCartMessage(productId);

        //when
        listener.onShoppingCartMessage(message);

        //then
        service.persistProduct(productId);
    }
}
