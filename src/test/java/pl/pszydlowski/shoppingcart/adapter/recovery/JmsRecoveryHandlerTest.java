package pl.pszydlowski.shoppingcart.adapter.recovery;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.Builder;
import lombok.Singular;
import org.apache.activemq.command.ActiveMQTextMessage;
import org.junit.Test;
import org.springframework.jms.core.BrowserCallback;
import org.springframework.jms.core.JmsTemplate;
import pl.pszydlowski.shoppingcart.adapter.health.DatabaseConnectionResumedEvent;
import pl.pszydlowski.shoppingcart.port.messaging.ShoppingCartMessage;

import javax.jms.JMSException;
import javax.jms.QueueBrowser;
import javax.jms.Session;
import java.io.IOException;
import java.util.Enumeration;
import java.util.List;
import java.util.NoSuchElementException;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.then;
import static org.mockito.Mockito.mock;

public class JmsRecoveryHandlerTest {

    private final JmsTemplate jmsTemplate = mock(JmsTemplate.class);
    private final ObjectMapper objectMapper = mock(ObjectMapper.class);
    private final JmsRecoveryHandler handler = new JmsRecoveryHandler(jmsTemplate, objectMapper);

    @Test
    public void should_resend_all_shopping_cart_messages_in_dead_letter_queue_and_ignore_different_messages() throws JMSException, IOException {
        //given
        String destination = "destination";

        DatabaseConnectionResumedEvent event = new DatabaseConnectionResumedEvent(handler);
        QueueBrowser browser = mock(QueueBrowser.class);

        String firstMessageText = "message-1";
        String secondMessageText = "message-2";
        String thirdMessageText = "message-3";

        ActiveMQTextMessage firstMQMessage = new ActiveMQTextMessage();
        firstMQMessage.setText(firstMessageText);

        ActiveMQTextMessage secondMQMessage = new ActiveMQTextMessage();
        secondMQMessage.setText(secondMessageText);

        ActiveMQTextMessage thirdMQMessage = new ActiveMQTextMessage();
        thirdMQMessage.setText(thirdMessageText);

        ListEnumeration enumeration = ListEnumeration.builder()
                .element(firstMQMessage)
                .element(secondMQMessage)
                .element(thirdMQMessage)
                .build();

        ShoppingCartMessage firstMessage = new ShoppingCartMessage("product-1");
        ShoppingCartMessage secondMessage = new ShoppingCartMessage("product-2");

        given(jmsTemplate.browse(eq("ActiveMQ.DLQ"), any())).willAnswer(invocation -> {
            ((BrowserCallback<Object>) invocation.getArgument(1)).doInJms(mock(Session.class), browser);
            return null;
        });

        given(objectMapper.readValue(firstMessageText, ShoppingCartMessage.class)).willReturn(firstMessage);
        given(objectMapper.readValue(secondMessageText, ShoppingCartMessage.class)).willReturn(secondMessage);
        given(objectMapper.readValue(thirdMessageText, ShoppingCartMessage.class)).willThrow(IOException.class);
        given(browser.getEnumeration()).willReturn(enumeration);

        handler.setShoppingCartDestination(destination);

        //when
        handler.onApplicationEvent(event);

        //then
        then(jmsTemplate).should().browse(eq("ActiveMQ.DLQ"), any());
        then(jmsTemplate).should().convertAndSend(destination, firstMessage);
        then(jmsTemplate).should().convertAndSend(destination, secondMessage);
        then(jmsTemplate).shouldHaveNoMoreInteractions();
    }

    @Builder
    private static class ListEnumeration implements Enumeration {

        @Singular
        private final List<Object> elements;
        private int index;

        public boolean hasMoreElements() {
            return index < elements.size();
        }

        public Object nextElement() {
            if (index < elements.size()) {
                return elements.get(index++);
            }
            throw new NoSuchElementException();
        }

    }
}
