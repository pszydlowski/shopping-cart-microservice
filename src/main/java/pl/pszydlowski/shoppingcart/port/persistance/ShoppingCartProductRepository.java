package pl.pszydlowski.shoppingcart.port.persistance;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.pszydlowski.shoppingcart.domain.ShoppingCartProduct;

@Repository
public interface ShoppingCartProductRepository extends JpaRepository<ShoppingCartProduct, String> {
}
