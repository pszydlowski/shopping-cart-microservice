package pl.pszydlowski.shoppingcart.port.messaging;

import lombok.Value;

@Value
public final class ShoppingCartMessage {
    String productId;
}
