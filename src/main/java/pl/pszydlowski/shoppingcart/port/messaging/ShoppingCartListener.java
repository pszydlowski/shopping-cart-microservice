package pl.pszydlowski.shoppingcart.port.messaging;

import org.springframework.stereotype.Service;

@Service
public interface ShoppingCartListener {
    void onShoppingCartMessage(ShoppingCartMessage message);
}
