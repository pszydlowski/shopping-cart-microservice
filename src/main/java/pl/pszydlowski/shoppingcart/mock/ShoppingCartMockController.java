package pl.pszydlowski.shoppingcart.mock;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.web.bind.annotation.*;
import pl.pszydlowski.shoppingcart.domain.ShoppingCartProduct;
import pl.pszydlowski.shoppingcart.port.messaging.ShoppingCartMessage;
import pl.pszydlowski.shoppingcart.port.persistance.ShoppingCartProductRepository;

import java.util.List;

@RestController
@RequestMapping("/shopping-cart")
class ShoppingCartMockController {

    private final ShoppingCartProductRepository customerShoppingCartRepository;
    private final String shoppingCartDestination;
    private final JmsTemplate jmsTemplate;

    ShoppingCartMockController(ShoppingCartProductRepository customerShoppingCartRepository,
                               @Value("${shopping-cart.messaging.destination}") String shoppingCartDestination,
                               JmsTemplate jmsTemplate) {
        this.customerShoppingCartRepository = customerShoppingCartRepository;
        this.shoppingCartDestination = shoppingCartDestination;
        this.jmsTemplate = jmsTemplate;
    }

    @GetMapping
    public List<ShoppingCartProduct> getProducts() {
        return customerShoppingCartRepository.findAll();
    }

    @PostMapping("/send-message")
    public void sendShoppingCartMessage(@RequestBody ShoppingCartMessage message) {
        jmsTemplate.convertAndSend(shoppingCartDestination, message);
    }

    @PostMapping("/send-multiple-messages")
    public void sendMultipleCartMessages(@RequestParam int messageCount) {
        for (int id = 0; id < messageCount; id++) {
            jmsTemplate.convertAndSend(shoppingCartDestination, new ShoppingCartMessage("product-" + id));
        }
    }

}
