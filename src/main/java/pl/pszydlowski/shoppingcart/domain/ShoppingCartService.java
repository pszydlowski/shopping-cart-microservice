package pl.pszydlowski.shoppingcart.domain;

import org.springframework.stereotype.Service;

@Service
public interface ShoppingCartService {
    void persistProduct(String productId);
}
