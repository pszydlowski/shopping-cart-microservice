package pl.pszydlowski.shoppingcart.domain;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import pl.pszydlowski.shoppingcart.port.persistance.ShoppingCartProductRepository;

@Component
@RequiredArgsConstructor
class DefaultShoppingCartService implements ShoppingCartService {

    private final ShoppingCartProductRepository repository;

    @Override
    @Transactional
    public void persistProduct(String productId) {
        ShoppingCartProduct product = ShoppingCartProduct
                .builder()
                .productId(productId)
                .build();

        repository.save(product);
    }
}
