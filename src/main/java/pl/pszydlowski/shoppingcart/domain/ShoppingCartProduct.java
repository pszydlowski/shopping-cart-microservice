package pl.pszydlowski.shoppingcart.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder(toBuilder = true)
@Entity
@Table(name = "shopping_cart_product")
public final class ShoppingCartProduct {
    @Id
    @GeneratedValue
    @Column(name = "id")
    private long id;
    @Column(name = "product_id")
    private String productId;
}
