package pl.pszydlowski.shoppingcart.adapter.messaging;

import lombok.RequiredArgsConstructor;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;
import pl.pszydlowski.shoppingcart.domain.ShoppingCartService;
import pl.pszydlowski.shoppingcart.port.messaging.ShoppingCartListener;
import pl.pszydlowski.shoppingcart.port.messaging.ShoppingCartMessage;

@Component
@RequiredArgsConstructor
class ShoppingCartJmsListener implements ShoppingCartListener {

    private final ShoppingCartService shoppingCartService;

    @JmsListener(destination = "${shopping-cart.messaging.destination}")
    public void onShoppingCartMessage(ShoppingCartMessage message) {
        shoppingCartService.persistProduct(message.getProductId());
    }

}
