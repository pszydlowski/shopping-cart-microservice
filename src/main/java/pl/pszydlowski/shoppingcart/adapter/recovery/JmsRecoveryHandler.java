package pl.pszydlowski.shoppingcart.adapter.recovery;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;
import org.apache.activemq.command.ActiveMQTextMessage;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationListener;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Component;
import pl.pszydlowski.shoppingcart.adapter.health.DatabaseConnectionResumedEvent;
import pl.pszydlowski.shoppingcart.port.messaging.ShoppingCartMessage;

import javax.jms.JMSException;
import java.io.IOException;
import java.util.Enumeration;

@Component
@Log4j2
@RequiredArgsConstructor
public class JmsRecoveryHandler implements ApplicationListener<DatabaseConnectionResumedEvent> {

    private final JmsTemplate jmsTemplate;
    private final ObjectMapper objectMapper;

    @Value("${shopping-cart.messaging.destination}")
    @Setter(AccessLevel.PACKAGE)
    private String shoppingCartDestination;

    @Override
    public void onApplicationEvent(DatabaseConnectionResumedEvent event) {
        log.info("Database connection has been resumed. Resending all messages present in dead letter queue.");
        jmsTemplate.browse("ActiveMQ.DLQ", (session, browser) -> {
            Enumeration messages = browser.getEnumeration();
            while (messages.hasMoreElements()) {
                resendMessage((ActiveMQTextMessage) messages.nextElement());
            }
            return null;
        });
    }

    private void resendMessage(ActiveMQTextMessage message) throws JMSException {
        String messageAsText = message.getText();
        try {
            ShoppingCartMessage receivedMessage = objectMapper.readValue(messageAsText, ShoppingCartMessage.class);
            log.info("Resending message <{}>", receivedMessage);
            jmsTemplate.convertAndSend(shoppingCartDestination, receivedMessage);
        } catch (IOException e) {
            log.warn("Failed to deserialize message into correct type, dead letter queue might be corrupted");
            e.printStackTrace();
        }
    }
}
