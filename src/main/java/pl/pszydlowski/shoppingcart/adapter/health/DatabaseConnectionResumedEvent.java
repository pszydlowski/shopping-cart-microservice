package pl.pszydlowski.shoppingcart.adapter.health;

import lombok.EqualsAndHashCode;
import org.springframework.context.ApplicationEvent;

@EqualsAndHashCode(callSuper = false)
public final class DatabaseConnectionResumedEvent extends ApplicationEvent {
    public DatabaseConnectionResumedEvent(Object source) {
        super(source);
    }
}
