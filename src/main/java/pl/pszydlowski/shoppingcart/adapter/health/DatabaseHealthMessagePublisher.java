package pl.pszydlowski.shoppingcart.adapter.health;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;
import org.springframework.boot.actuate.health.Health;
import org.springframework.boot.actuate.health.Status;
import org.springframework.boot.actuate.jdbc.DataSourceHealthIndicator;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
class DatabaseHealthMessagePublisher {

    private final DataSourceHealthIndicator indicator;
    private final ApplicationEventPublisher publisher;

    @Getter(AccessLevel.PACKAGE)
    @Setter(AccessLevel.PACKAGE)
    private boolean isDatabaseActive = true;

    @Scheduled(fixedDelay = 5000)
    public void checkDatabaseHealth() {
        Health health = indicator.health();
        Status status = health.getStatus();
        if (status != null) {
            if (isDatabaseActive && status.getCode().equals("DOWN")) {
                isDatabaseActive = false;
            }
            if (!isDatabaseActive && status.getCode().equals("UP")) {
                isDatabaseActive = true;
                publisher.publishEvent(new DatabaseConnectionResumedEvent(this));
            }
        }
    }
}
